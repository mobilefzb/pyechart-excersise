# pyechart-excersise
> 一个 flask restful 加 pyechart 的小练习，实现一套 restful 接口来展示图标数据。

# 开发环境
> python 3.x

# 需求包
|Package|Version|
|:-----:|:-----:|
|Flask|0.12.2|
|Flask-RESTful|0.3.6|
|pyecharts|0.3.1|

